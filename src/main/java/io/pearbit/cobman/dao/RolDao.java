package io.pearbit.cobman.dao;

import io.pearbit.cobman.models.Rol;
/**
 * To be implemented by any class which want to figure out like a DAO for Rol entity
 * @author JuanBG
 *
 */
public interface RolDao {
	/**
	 * fin by name
	 * @param name
	 * @return
	 */
	public Rol findByName(String name);
	/**
	 * save new rol
	 * @param rol
	 */
	public void save(Rol rol);
}
