package io.pearbit.cobman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.pearbit.cobman.controller.UserService;
/**
 * Start point according with use case design planed by COBMAN TEAM {@link https://docs.google.com/document/d/1OkiSaIA6A5O5OQ0ieuhrdHQ0pwE7Jqpb89ks_v5P8tg/edit}
 * The first entry point are "Generals" but it will be performed by UI completely.
 * The next point are "Two factor validation" but it will be performed by PearBit Master Project Validation System, If all is good, The MPVS 
 * will call the {@link UserService} for createMasterUser() method, 
 * 
 * So the next entry point is irrelevant 'cause, they are services to make registry and start with the management. 
 * Please refer to the above link to see the detail. 
 * 
 * @author JuanBG
 *
 */
@SpringBootApplication
public class CobmanApplication {

	
	
	
	public static void main(String[] args) {
		SpringApplication.run(CobmanApplication.class, args);
	}
}
